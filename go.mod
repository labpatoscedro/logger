module bitbucket.org/labpatoscedro/logger

go 1.15

require (
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.16.0
	golang.org/x/tools v0.0.0-20191112195655-aa38f8e97acc // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
