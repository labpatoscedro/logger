package logger_test

import (
	"testing"

	"bitbucket.org/labpatoscedro/logger"
)

func BenchmarkLogger(b *testing.B) {
	l := logger.NewLoggerWithLevel(logger.JSONEncoding, logger.InfoLevel)

	for i := 0; i < b.N; i++ {
		l.Info("my info msg")
	}
}

func ExampleLogger() {
	log := logger.NewLoggerWithLevel(logger.JSONEncoding, logger.DebugLevel)
	defer log.Sync()

	log.Debug("my log message")
	log.Debug("my log message",
		"key1", "value 1",
		"key2", 1,
		"key3", 1.2,
		"key4", true,
		"key5", map[string]interface{}{
			"key":         "value",
			"another_key": 1,
		})

	log.Info("my log message")
	log.Info("my log message",
		"key1", "value 1",
		"key2", 1,
		"key3", 1.2,
		"key4", true,
		"key5", map[string]interface{}{
			"key":         "value",
			"another_key": 1,
		})

	log.Warn("my log message")
	log.Warn("my log message",
		"key1", "value 1",
		"key2", 1,
		"key3", 1.2,
		"key4", true,
		"key5", map[string]interface{}{
			"key":         "value",
			"another_key": 1,
		})

	log.Error("my log message")
	log.Error("my log message",
		"key1", "value 1",
		"key2", 1,
		"key3", 1.2,
		"key4", true,
		"key5", map[string]interface{}{
			"key":         "value",
			"another_key": 1,
		})

	log.DPanic("my log message")
	log.DPanic("my log message",
		"key1", "value 1",
		"key2", 1,
		"key3", 1.2,
		"key4", true,
		"key5", map[string]interface{}{
			"key":         "value",
			"another_key": 1,
		})

	log.Panic("my log message")
	log.Panic("my log message",
		"key1", "value 1",
		"key2", 1,
		"key3", 1.2,
		"key4", true,
		"key5", map[string]interface{}{
			"key":         "value",
			"another_key": 1,
		})

	log.Fatal("my log message")
	log.Fatal("my log message",
		"key1", "value 1",
		"key2", 1,
		"key3", 1.2,
		"key4", true,
		"key5", map[string]interface{}{
			"key":         "value",
			"another_key": 1,
		})
}

func ExampleNew() {
	log := logger.New()
	defer log.Sync()

	log.Info("info message")
}

func ExampleNewLogger() {
	jsonLogger := logger.NewLogger(logger.JSONEncoding)
	defer jsonLogger.Sync()

	jsonLogger.Info("info message")

	consoleLogger := logger.NewLogger(logger.ConsoleEncoding)
	defer consoleLogger.Sync()

	consoleLogger.Info("info message")
}

func ExampleNewLoggerWithLevel() {
	_ = logger.NewLoggerWithLevel(logger.JSONEncoding, logger.DebugLevel)
	_ = logger.NewLoggerWithLevel(logger.JSONEncoding, logger.InfoLevel)
	_ = logger.NewLoggerWithLevel(logger.JSONEncoding, logger.WarnLevel)
	_ = logger.NewLoggerWithLevel(logger.JSONEncoding, logger.ErrorLevel)
	_ = logger.NewLoggerWithLevel(logger.JSONEncoding, logger.DPanicLevel)
	_ = logger.NewLoggerWithLevel(logger.JSONEncoding, logger.PanicLevel)
	_ = logger.NewLoggerWithLevel(logger.JSONEncoding, logger.FatalLevel)

	_ = logger.NewLoggerWithLevel(logger.ConsoleEncoding, logger.DebugLevel)
	_ = logger.NewLoggerWithLevel(logger.ConsoleEncoding, logger.InfoLevel)
	_ = logger.NewLoggerWithLevel(logger.ConsoleEncoding, logger.WarnLevel)
	_ = logger.NewLoggerWithLevel(logger.ConsoleEncoding, logger.ErrorLevel)
	_ = logger.NewLoggerWithLevel(logger.ConsoleEncoding, logger.DPanicLevel)
	_ = logger.NewLoggerWithLevel(logger.ConsoleEncoding, logger.PanicLevel)
	_ = logger.NewLoggerWithLevel(logger.ConsoleEncoding, logger.FatalLevel)
}

func ExampleNil() {
	log := logger.Nil()

	log.Info("silent info message")
	log.Debug("silent debug message")
}
